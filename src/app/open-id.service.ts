import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Identity } from './identity';
import { ConfigService } from './config.service';
import { Router } from '@angular/router';

@Injectable()
export class OpenIdService {
  params: any;
  reference_string: any; 

  constructor(private http: HttpClient,
    private config: ConfigService,
    private router: Router) {
    this.params = {};
    this.reference_string = "";
  }

  login(identity: Identity) {
    const httpOptions = {
      withCredentials: true
    };
    return this.http.post(this.config.get().apiUrl + '/openid/login', { 'identity': identity.pubkey}, httpOptions);
  }

  parseRouteParams(params: any): any {
    this.params = params;
    console.log('Added OIDC params');
    console.log(this.params);
  }

  authorize(): any {
    window.location.href = this.config.get().apiUrl + '/openid/authorize?client_id=' + this.params.get('client_id') +
    '&redirect_uri=' + this.params.get('redirect_uri') +
    '&response_type=' + this.params.get('response_type') +
    '&scope=' + this.params.get('scope') + " " + this.reference_string +
    '&state=' + this.params.get('state') +
    '&code_challenge=' + this.params.get('code_challenge') +
    '&nonce=' + this.params.get('nonce');
  }

  setReferences(references) {
    for(var i =0; i<references.length; i++){
      this.reference_string= this.reference_string + references[i].name+ " "
    }
  }

  cancelAuthorization(): any {
    const httpOptions = {
      withCredentials: true
    };
    this.params = {};
    return this.http.post(this.config.get().apiUrl + '/openid/login', { 'identity': 'Denied'}, httpOptions);
  }

  inOpenIdFlow(): any {
    if (this.params.get('redirect_uri') !== null) {
      console.log ('In OIDC flow');
    } else {
      console.log ('Not in OIDC flow');
      console.log (this.params);
    }
    return this.params.get('redirect_uri') !== null;
  }

  getClientId(): any {
    if (!this.inOpenIdFlow()) {
      return '';
    }
    return this.params.get('client_id');
  }

  getScope(): any {
    if (!this.inOpenIdFlow()) {
      return [];
    }
    const scopes = this.params.get('scope').split(' ');
    const i = scopes.indexOf('openid');
    scopes.splice(i, 1);
    return scopes;
  }

  getRefScope(): any {
    if (!this.inOpenIdFlow()) {
      return [];
    }
    var scope = [];
    var json = JSON.parse(this.params.get('claims'))['userinfo'];
    for(var key in json)
    {
      if (json[key]['attestation'] === true)
        {
          scope.push([key, json[key]['essential'], json[key]['attestation'], json[key]['format']]);
        }
    }
    return scope;
  }
}
