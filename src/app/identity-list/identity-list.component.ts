import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Attribute } from '../attribute';
import { Reference } from '../reference';
import { Attestation } from '../attestation';
import { GnsService } from '../gns.service';
import { Identity } from '../identity';
import { IdentityService } from '../identity.service';
import { NamestoreService } from '../namestore.service';
import { OpenIdService } from '../open-id.service';
import { ReclaimService } from '../reclaim.service';
import { ModalService } from '../modal.service';
import { finalize } from 'rxjs/operators';
import { from, forkJoin, EMPTY } from 'rxjs';

@Component({
  selector: 'app-identity-list',
  templateUrl: './identity-list.component.html',
  styleUrls: ['./identity-list.component.scss']
})

export class IdentityListComponent implements OnInit {

  requestedAttributes: any;
  missingAttributes: any;
  attributes: any;
  requestedReferences: any;
  missingReferences: any;
  references: any;
  newReference: Reference;
  attestation: any;
  attestation_val: any;
  newAttestation: Attestation;
  optionalReferences: any;
  tickets: any;
  clientName: any;
  identities: Identity[];
  newIdentity: Identity;
  newAttribute: Attribute;
  identityInEdit: Identity;
  identityInEditName: string;
  identityNameMapper: any;
  showTicketsIdentity: Identity;
  showReferenceMan: Identity;
  showAttestationMan: Identity;
  showConfirmDelete: any;
  showConfirmRevoke: any;
  connected: any;
  ticketAttributeMapper: any;
  modalOpened: any;
  clientNameFound: any;
  errorInfos: any;
  objectKeys : any;

  constructor(private route: ActivatedRoute, private oidcService: OpenIdService,
    private identityService: IdentityService,
    private reclaimService: ReclaimService,
    private namestoreService: NamestoreService,
    private gnsService: GnsService,
    private modalService: ModalService,
    private router: Router) {
  }

  ngOnInit() {
    this.objectKeys= Object.keys;
    this.attributes = {};
    this.references = {};
    this.missingReferences = {};
    this.requestedReferences = {};
    this.newReference = new Reference('', '', '', '');
    this.attestation = {};
    this.attestation_val = {};
    this.newAttestation = new Attestation('', '', '', '');
    this.optionalReferences = {};  
    this.tickets = {};
    this.identities = [];
    this.showConfirmDelete = null;
    this.showConfirmRevoke = null;
    this.newAttribute = new Attribute('', '', '', 'STRING', '');
    this.requestedAttributes = {};
    this.missingAttributes = {};
    this.clientName = '-';
    this.connected = false;
    this.ticketAttributeMapper = {};
    this.modalOpened = false;
    this.oidcService.parseRouteParams(this.route.snapshot.queryParamMap);
    this.getClientName();
    this.identityInEditName = '';
    this.identityNameMapper = {};
    this.updateIdentities();
    this.errorInfos = [];
    console.log('processed nginit');
  }

  confirmDelete(identity) { this.showConfirmDelete = identity; }

  confirmRevoke(ticket) { this.showConfirmRevoke = ticket; }

  hideConfirmDelete() { this.showConfirmDelete = null; }

  hideConfirmRevoke() { this.showConfirmRevoke = null; }

  getClientName() {
    this.clientNameFound = undefined;
    this.clientName = this.oidcService.getClientId();
    if (!this.oidcService.inOpenIdFlow()) {
      return;
    }
    this.gnsService.getClientName(this.oidcService.getClientId())
      .subscribe(record => {
        const records = record.data;
        console.log(records);
        for (let i = 0; i < records.length; i++) {
          if (records[i].record_type !== 'RECLAIM_OIDC_CLIENT') {
            continue;
          }
          this.clientName = records[i].value;
          this.clientNameFound = true;
          return;
        }
        this.clientNameFound = false;
      }, err => {
        console.log(err);
        this.clientNameFound = false;
      });
  }

  intToRGB(i) {
    i = this.hashCode(i);
    const c = (i & 0x00FFFFFF).toString(16).toUpperCase();

    return '#' +
      '00000'.substring(0, 6 - c.length) + c;
  }

  hashCode(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  isAddIdentity() { return null != this.newIdentity; }

  isDuplicate() {
    for (let i = 0; i < this.identities.length; i++) {
      if (this.identities[i].name === this.newIdentity.name) {
        return true;
      }
    }
  }

  canSave() {
    if (this.newIdentity.name == null) {
      return false;
    }
    if (this.newIdentity.name === '') {
      return false;
    }
    if (!/^[a-zA-Z0-9-]+$/.test(this.newIdentity.name)) {
      return false;
    }
    if (this.isDuplicate()) {
      return false;
    }
    return true;
  }

  addIdentity() { this.newIdentity = new Identity('', ''); }

  editIdentity(identity) {
    this.identityInEdit = identity;
    this.showTicketsIdentity = null;
    this.showReferenceMan = null;
    this.showAttestationMan = null;
  }

  isInEdit(identity) { return this.identityInEdit === identity; }

  saveIdentityValues(identity) {
    this.saveIdentityAttributes(identity);
    this.saveIdentityReferences(identity);
    this.saveIdentityAttestation(identity);
    this.identityInEdit = null;
  }

  private saveIdentityAttributes(identity) {
    this.storeAttributes(identity)
      .pipe(
        finalize(() => {
          this.newAttribute.name = '';
          this.newAttribute.value = '';
          this.newAttribute.type = 'STRING';
        }))
      .subscribe(res => {
        //FIXME success dialog/banner
        this.updateAttributes(identity);
      },
      err => {
        console.log(err);
        this.errorInfos.push("Failed to update identity ``" +  this.identityInEdit.name + "''");
      });
  }

  private saveIdentityReferences(identity) {
    this.storeReferences(identity)
      .pipe(
        finalize(() => {
          this.newReference.name = '';
          this.newReference.ref_value = '';
          this.newReference.ref_id = '';
        }))
      .subscribe(res => {
        //FIXME success dialog/banner
        this.updateReferences(identity);
      },
      err => {
        console.log(err);
        this.errorInfos.push("Failed to update identity ``" +  this.identityInEdit.name + "''");
      });
  }

  private saveIdentityAttestation(identity) {
    this.storeAttestation(identity)
      .pipe(
        finalize(() => {
          this.newAttestation.name = '';
          this.newAttestation.value = '';
          this.newAttestation.type = '';
        }))
      .subscribe(res => {
        //FIXME success dialog/banner
        this.updateAttestation(identity);
      },
      err => {
        console.log(err);
        this.errorInfos.push("Failed to update identity ``" +  this.identityInEdit.name + "''");
      });
  }

  discardIdentity(identity) {
    this.identityInEdit = null;
    this.newAttribute.name = '';
    this.newAttribute.value = '';
    this.newAttribute.type = 'STRING';
    this.newReference.name = '';
    this.newReference.ref_value = '';
    this.newReference.ref_id = '';
    this.newAttestation.name = '';
    this.newAttestation.value = '';
    this.newAttestation.type = '';
  }

  deleteAttribute(attribute) {
    this.reclaimService.deleteAttribute(this.identityInEdit, attribute)
      .subscribe(res => {
        //FIXME info dialog
        this.updateAttributes(this.identityInEdit);
      },
      err => {
        this.errorInfos.push("Failed to delete attribute ``" + attribute.name + "''");
        console.log(err);
      });
  }

  deleteReference(reference) {
    this.reclaimService.deleteReference(this.identityInEdit, reference)
      .subscribe(res => {
        //FIXME info dialog
        this.updateReferences(this.identityInEdit);
        this.updateAttributes(this.identityInEdit);
      },
      err => {
        this.errorInfos.push("Failed to delete reference ``" + reference.name + "''");
        console.log(err);
      });
  }

  deleteAttestation(attestation) {
    this.reclaimService.deleteAttestation(this.identityInEdit, attestation)
      .subscribe(res => {
        //FIXME info dialog
        this.updateAttestation(this.identityInEdit);
        this.updateReferences(this.identityInEdit);
        this.updateAttributes(this.identityInEdit);
      },
      err => {
        this.errorInfos.push("Failed to delete attestation ``" + attestation.name + "''");
        console.log(err);
      });
  }

  getMissingAttributes(identity) {
    const scopes = this.getScopes();
    let i;
    for (i = 0; i < this.requestedAttributes[identity.pubkey].length; i++) {
      const j =
        scopes.indexOf(this.requestedAttributes[identity.pubkey][i].name);
      if (j >= 0) {
        scopes.splice(j, 1);
      }
    }
    this.missingAttributes[identity.pubkey] = [];
    for (i = 0; i < scopes.length; i++) {
      const attribute = new Attribute('', '', '', 'STRING', '');
      attribute.name = scopes[i];
      this.missingAttributes[identity.pubkey].push(attribute);
    }
  }

  getMissingReferences(identity) {
    const refscopes = this.getRefScope();
    let i;
    for (i = 0; i < this.requestedReferences[identity.pubkey].length; i++) {
      for (var j = 0; j < refscopes.length; j++) {
        if (this.requestedReferences[identity.pubkey][i].name === refscopes[j][0] ) {
          refscopes.splice(j,1);
        }
      }
    }
    this.missingReferences[identity.pubkey] = [];
    this.optionalReferences[identity.pubkey] = [];
    for (i = 0; i < refscopes.length; i++) {
      const reference = new Reference('', '', '', '');
      if (refscopes[i][1] === true)
      {
        reference.name = refscopes[i][0];
        this.missingReferences[identity.pubkey].push(reference);
      }
      if (refscopes[i][1] === false)
      {
        reference.name = refscopes[i][0];
        this.optionalReferences[identity.pubkey].push(reference);
      }
    }
  }

  private mapAudience(ticket) {
    this.gnsService.getClientName(ticket.audience).subscribe(records => {
      for (let i = 0; i < records.data.length; i++) {
        if (records.data[i].record_type !== 'RECLAIM_OIDC_CLIENT') {
          continue;
        }
        this.identityNameMapper[ticket.audience] = records.data[i].value;
        break;
      }
    });
  }

  private mapAttributes(identity, ticket) {
    this.namestoreService.getNames(identity).subscribe(names => {
      this.ticketAttributeMapper[ticket.audience] = [];
      names = names.filter(name => name.record_name === ticket.rnd.toLowerCase());
      for (let i = 0; i < names.length; i++) {
        names[i].data.forEach(record => {
          if (record.record_type === 'RECLAIM_ATTR_REF') {
            this.attributes[identity.pubkey]
              .filter(attr => attr.id === record.value)
              .map(attr => {
                this.ticketAttributeMapper[ticket.audience].push(attr.name);
              });
          }
        });
      }
    });
  }

  private updateTickets(identity) {
    this.reclaimService.getTickets(identity).subscribe(tickets => {
      this.tickets[identity.pubkey] = [];
      if (tickets === null) {
        return;
      }
      this.tickets[identity.pubkey] = tickets;
      tickets.forEach(ticket => {
        this.mapAudience(ticket);
        this.mapAttributes(identity, ticket);
      });
    },
    err => {
      this.errorInfos.push("Unable to retrieve tickets for identity ``" + identity.name + "''");
      console.log(err);
    });
  }

  toggleShowTickets(identity) {
    if (this.showTicketsIdentity === identity) {
      this.showTicketsIdentity = null;
      return;
    }
    this.showTicketsIdentity = identity;
  }

  toggleShowRef(identity) {
    if (this.showReferenceMan === identity) {
      this.showReferenceMan = null;
      return;
    }
    this.showReferenceMan = identity;
  }

  toggleShowAttest(identity) {
    if (this.showAttestationMan === identity) {
      this.showAttestationMan = null;
      return;
    }
    this.showAttestationMan = identity;
  }

  revokeTicket(identity, ticket) {
    this.reclaimService.revokeTicket(ticket).subscribe(
      result => {
        this.updateAttributes(identity);
      },
      err => {
        this.errorInfos.push("Unable to revoke ticket.");
        console.log(err);
      });
  }


  private updateAttributes(identity) {
    this.reclaimService.getAttributes(identity).subscribe(attributes => {
      this.attributes[identity.pubkey] = [];
      this.requestedAttributes[identity.pubkey] = [];
      if (attributes === null) {
        this.getMissingAttributes(identity);
        return;
      }
      let i;
      for (i = 0; i < attributes.length; i++) {
        this.attributes[identity.pubkey].push(attributes[i]);
        if (this.oidcService.getScope().includes(attributes[i].name)) {
          this.requestedAttributes[identity.pubkey].push(attributes[i]);
        }
      }
      this.getMissingAttributes(identity);
      this.updateTickets(identity);
    },
    err => {
      this.errorInfos.push("Error retrieving attributes for ``" + identity.name + "''");
      console.log(err);
    });
  }

  private updateReferences(identity) {
    this.reclaimService.getReferences(identity).subscribe(references => {
      this.references[identity.pubkey] = [];
      this.requestedReferences[identity.pubkey] = [];
      if (references === null) {
        this.getMissingReferences(identity);
        return;
      }
     const scope = this.oidcService.getRefScope();
      let i;
      for (i = 0; i < references.length; i++) {
        this.references[identity.pubkey].push(references[i]);
        let j;
        for (j = 0; j < scope.length; j++) {
          if (references[i].name === scope[j][0] ) {
            this.requestedReferences[identity.pubkey].push(references[i]);
          }
        }
      }
      this.getMissingReferences(identity);
      this.updateTickets(identity);
    },
    err => {
      this.errorInfos.push("Error retrieving references for ``" + identity.name + "''");
      console.log(err);
    });
  }

  private updateAttestation(identity) {
    this.reclaimService.getAttestation(identity).subscribe(attestation => {
      this.attestation[identity.pubkey] = [];
      this.attestation_val[identity.pubkey] = [];
      let i;
      for (i = 0; i < attestation.length; i++) {
        this.attestation[identity.pubkey].push(attestation[i]);
        this.setAttestationValue(attestation[i], identity);
      }
      this.updateTickets(identity);
    },
    err => {
      this.errorInfos.push("Error retrieving attestation for ``" + identity.name + "''");
      console.log(err);
    });
  }

  private storeAttributes(identity) {
    const promises = [];
    let i;
    if (undefined !== this.missingAttributes[identity.pubkey]) {
      for (i = 0; i < this.missingAttributes[identity.pubkey].length; i++) {
        if (this.missingAttributes[identity.pubkey][i].value === '') {
          continue;
        }
        promises.push(from(this.reclaimService.addAttribute(
          identity, this.missingAttributes[identity.pubkey][i])));
      }
    }
    if (undefined !== this.attributes[identity.pubkey]) {
      for (i = 0; i < this.attributes[identity.pubkey].length; i++) {
        if (this.attributes[identity.pubkey][i].flag === '1') {
          continue;
        }
        promises.push(
          from(this.reclaimService.addAttribute(identity, this.attributes[identity.pubkey][i])));
      }
    }
    if (this.newAttribute.value !== '') {
      promises.push(from(this.reclaimService.addAttribute(identity, this.newAttribute)));
    }

    return forkJoin(promises);
  }


  private storeReferences(identity) {
    const promises = [];
    let i;
    if (undefined !== this.missingReferences[identity.pubkey]) {
      for (i = 0; i < this.missingReferences[identity.pubkey].length; i++) {
        if ((this.missingReferences[identity.pubkey][i].ref_value === '') || (this.missingReferences[identity.pubkey][i].ref_id === '')) {
          console.log("EmptyReferences: " + this.missingReferences[identity.pubkey][i]);
          continue;
        }
        console.log("MissingReferences: " + this.missingReferences[identity.pubkey][i]);
        promises.push(from(this.reclaimService.addReference(
          identity, this.missingReferences[identity.pubkey][i])));
      }
    }
    if (undefined !== this.references[identity.pubkey]) {
      for (i = 0; i < this.references[identity.pubkey].length; i++) {
        promises.push(
          from(this.reclaimService.addReference(identity, this.references[identity.pubkey][i])));
      }
    }
    if ((this.newReference.ref_value !== '') || (this.newReference.ref_id !== '')) {
      promises.push(from(this.reclaimService.addReference(identity, this.newReference)));
    }

    return forkJoin(promises);
  }

  private storeAttestation(identity) {
    const promises = [];
    let i;
    if (undefined !== this.attestation[identity.pubkey]) {
      for (i = 0; i < this.attestation[identity.pubkey].length; i++) {
        promises.push(
          from(this.reclaimService.addAttestation(identity, this.attestation[identity.pubkey][i])));
      }
    }
    if ((this.newAttestation.value !== '') || (this.newAttestation.type !== '')) {
      promises.push(from(this.reclaimService.addAttestation(identity, this.newAttestation)));
    }
    return forkJoin(promises);
  }

  addAttribute() {
    this.storeAttributes(this.identityInEdit)
      .pipe(
        finalize(() => {
          this.newAttribute.name = '';
          this.newAttribute.value = '';
          this.newAttribute.type = 'STRING';
          this.updateAttributes(this.identityInEdit);
        }))
      .subscribe(res => {
        console.log(res);
      },
      err => {
        console.log(err);
        this.errorInfos.push("Failed to update identity ``" +  this.identityInEdit.name + "''");
        EMPTY
      });
  }

  addReference() {
    this.storeReferences(this.identityInEdit)
      .pipe(
        finalize(() => {
          this.newReference.name = '';
          this.newReference.ref_value= '';
          this.newReference.ref_id = '';
          this.updateReferences(this.identityInEdit);
          this.updateAttributes(this.identityInEdit);
        }))
      .subscribe(res => {
        console.log(res);
      },
      err => {
        console.log(err);
        this.errorInfos.push("Failed to update identity ``" +  this.identityInEdit.name + "''");
        EMPTY
      });
  }

  addAttestation() {
    this.storeAttestation(this.identityInEdit)
      .pipe(
        finalize(() => {
          this.newAttestation.name = '';
          this.newAttestation.type = '';
          this.newAttestation.value = '';
          this.updateAttestation(this.identityInEdit);
        }))
      .subscribe(res => {
        console.log(res);
      },
      err => {
        console.log(err);
        this.errorInfos.push("Failed to update identity ``" +  this.identityInEdit.name + "''");
        EMPTY
      });
  }

  private setAttestationValue(attestation, identity) {
    var value_string="";
    return this.reclaimService.parseAttest(attestation).subscribe(json_string =>{
      this.attestation_val[identity.pubkey][attestation.id]=json_string;
    },
    err => {
      this.errorInfos.push("Error parsing attestation ``" + attestation.name + "''");
      console.log(err);
    });
  }

  isAttestationValid(attestation_id, identity) { 
    const now = Date.now().valueOf() / 1000;
    if (this.attestation_val[identity.pubkey][attestation_id]['exp'] === 'undefined') {
      return true;
    }
    return this.attestation_val[identity.pubkey][attestation_id]['exp'] > now;
  }

   addExampleAttestation() {
      var exampleAttestation = new Attestation('exampleattest', '', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJFeGFtcGxlIENBIiwiaWF0IjoxNTcwMzc1ODIxLCJleHAiOjE2MDE5MTE4MjEsImF1ZCI6IioiLCJzdWIiOiJGb29AYmFyLmNvbSIsIkdpdmVuTmFtZSI6IkZvbyIsIlN1cm5hbWUiOiJCYXIiLCJFbWFpbCI6IkZvb0BiYXIuY29tIn0.PZwmUoJk7RXMJXYY5RoAUYeiIwNjEPEYTY9zT9o8vR4', 'JWT');
      if (this.canAddAttestation(this.identityInEdit, exampleAttestation)){
        this.reclaimService.addAttestation(this.identityInEdit, exampleAttestation)
          .subscribe(res => {
            //FIXME info dialog
            this.updateAttestation(this.identityInEdit);
            this.updateReferences(this.identityInEdit);
            this.updateAttributes(this.identityInEdit);
          },
          err => {
            this.errorInfos.push("Failed to add attestation ``" + exampleAttestation.name + "''");
            console.log(err);
          });
      }
    }

  cancelAddIdentity() { this.newIdentity = null; }

  saveIdentity() {
    if (!this.canSave()) {
      return;
    }
    this.identityInEditName = this.newIdentity.name;
    this.identityService.addIdentity(this.newIdentity)
      .subscribe(res => {
        this.newIdentity.name = '';
        this.updateIdentities();
        this.cancelAddIdentity();
      },
      err => {
        this.errorInfos.push("Failed adding new identity ``" + this.newIdentity.name + "''");
        console.log(err);
      });
  }

  deleteIdentity(identity) {
    this.showConfirmDelete = false;
    this.identityInEdit = null;
    this.identityService.deleteIdentity(identity.pubkey)
      .subscribe(res => {
        this.updateIdentities();
      },
      err => {
        this.errorInfos.push("Failed deleting identity ``" + identity.name + "''");
        console.log(err);
      });
  }

  cancelRequest() {
    this.closeModal('OpenIdInfo');
    this.modalOpened = false;
    this.oidcService.cancelAuthorization().subscribe(() => {
      console.log('Request cancelled');
      this.requestedAttributes = {};
      this.missingAttributes = {};
      this.requestedReferences = {};
      this.missingReferences = {};
      this.optionalReferences = {};
      this.router.navigate(['/']);
      //Manually reset this component
    });
  }

  loginIdentity(identity) {
    this.oidcService.setReferences(this.requestedReferences[identity.pubkey]);
    this.oidcService.login(identity).subscribe(() => {
      console.log('Successfully logged in');
      this.authorize();
    });
  }

  authorize() { this.oidcService.authorize(); }

  openModal(id: string) {
    this.modalService.open(id);
    this.modalOpened = true;
  }

  closeModal(id: string) {
    this.modalService.close(id);
    if (!this.inOpenIdFlow()) {
      this.modalOpened = false;
    }
  }


  inOpenIdFlow() {
    if (this.oidcService.inOpenIdFlow() && !this.modalOpened) {
      this.openModal('OpenIdInfo');
    }
    return this.oidcService.inOpenIdFlow();
  }

  hasFlag(attribute) {
    if (attribute.flag ==='1') {
      return true;
    }
    return false;
  }

  canAddAttribute(identity, attribute) {
    if ((attribute.name === '') || (attribute.value === '')) {
      return false;
    }
    if (attribute.name.indexOf(' ') >= 0) {
      return false;
    }
    return !this.isInConflict(identity, attribute);
  }

  canAddReference(identity, reference) {
    if ((reference.name === '') || (reference.ref_value === '') || (reference.ref_id === '')) {
      return false;
    }
    if (reference.name.indexOf(' ') >= 0) {
      return false;
    }
    return !this.isRefInConflict(identity, reference);
  }

  canAddAttestation(identity, attestation) {
    if ((attestation.name === '') || (attestation.value === '') || (attestation.type === '')) {
      return false;
    }
    if (attestation.name.indexOf(' ') >= 0) {
      return false;
    }
    return !this.isAttestInConflict(identity, attestation);
  }

  attributeNameValid(identity, attribute) {
    if (attribute.name === '' && attribute.value === '') {
      return true;
    }
    if (attribute.name.indexOf(' ') >= 0) {
      return false;
    }
    if (!/^[a-zA-Z0-9-]+$/.test(attribute.name)) {
      return false;
    }
    return !this.isInConflict(identity, attribute);
  }

  attributeValueValid(attribute) {
    if (attribute.value === '') {
      return attribute.name === '';
    }
    return true;
  }

  referenceNameValid(identity, reference) {
    if (reference.name === '' && reference.ref_value === '' && reference.ref_id === '') {
      return true;
    }
    if (reference.name.indexOf(' ') >= 0) {
      return false;
    }
    if (!/^[a-zA-Z0-9-_]+$/.test(reference.name)) {
      return false;
    }
    return !this.isRefInConflict(identity, reference);
  }

  referenceValueValid(reference) {
    if (reference.ref_value === '') {
      return reference.name === '';
    }
    return true;
  }

  referenceIDValid(reference) {
    if (reference.ref_id === '') {
      return reference.name === '';
    }
    return true;
  }

  attestationNameValid(identity, attestation) {
    if (attestation.name === '' && attestation.value === '' && attestation.type === '') {
      return true;
    }
    if (attestation.name.indexOf(' ') >= 0) {
      return false;
    }
    if (!/^[a-zA-Z0-9-]+$/.test(attestation.name)) {
      return false;
    }
    return !this.isAttestInConflict(identity, attestation);
  }

  attestationTypeValid(attestation) {
    if (attestation.type === '') {
      return attestation.name === '';
    }
    return true;
  }

  attestationValueValid(attestation) {
    return true;
  }

  canSaveIdentity(identity) {
    return (this.canSaveAttrIdentity(identity) && 
            this.canSaveRefIdentity(identity)) && 
            this.canSaveAttestIdentity(identity);
  }

  private canSaveAttrIdentity(identity) {
    if (this.canAddAttribute(identity, this.newAttribute)) {
      return true;
    }
    return ((this.newAttribute.name === '') &&
      (this.newAttribute.value === '')) &&
      !this.isInConflict(identity, this.newAttribute);
  }

   private canSaveRefIdentity(identity) {
    if (this.canAddReference(identity, this.newReference)) {
      return true;
    }
    return ((this.newReference.name === '') && 
      (this.newReference.ref_value === '') && 
      (this.newReference.ref_id === '')) && 
      !this.isRefInConflict(identity, this.newReference);
  }

  private canSaveAttestIdentity(identity) {
    if (this.canAddAttestation(identity, this.newAttestation)) {
      return true;
    }
    return ((this.newAttestation.name === '') && 
      (this.newAttestation.value === '') && 
      (this.newAttestation.type === '')) && 
      !this.isAttestInConflict(identity, this.newAttestation);
  }

  isInConflict(identity, attribute) {
    let i;
    if (undefined !== this.missingAttributes[identity.pubkey]) {
      for (i = 0; i < this.missingAttributes[identity.pubkey].length; i++) {
        if (attribute.name ===
          this.missingAttributes[identity.pubkey][i].name) {
          return true;
        }
      }
    }
    if (undefined !== this.attributes[identity.pubkey]) {
      for (i = 0; i < this.attributes[identity.pubkey].length; i++) {
        if (attribute.name === this.attributes[identity.pubkey][i].name) {
          return true;
        }
      }
    }
    return false;
  }

  isRefInConflict(identity, reference) {
    let i;
    if (undefined !== this.missingReferences[identity.pubkey]) {
      for (i = 0; i < this.missingReferences[identity.pubkey].length; i++) {
        if (reference.name ===
          this.missingReferences[identity.pubkey][i].name) {
          return true;
        }
      }
    }
    if (undefined !== this.references[identity.pubkey]) {
      for (i = 0; i < this.references[identity.pubkey].length; i++) {
        if (reference.name === this.references[identity.pubkey][i].name) {
          return true;
        }
      }
    }
    return false;
  }

  isAttestInConflict(identity, attestation) {
    let i;
    if (undefined !== this.attestation[identity.pubkey]) {
      for (i = 0; i < this.attestation[identity.pubkey].length; i++) {
        if (attestation.name === this.attestation[identity.pubkey][i].name) {
          return true;
        }
      }
    }
    return false;
  }


  getScopes() { return this.oidcService.getScope(); }

  getRefScope() { return this.oidcService.getRefScope(); }

  getScopesPretty() { return this.getScopes().join(', '); }

  getMissing(identity) {
    const arr = [];
    let i = 0;
    for (i = 0; i < this.missingAttributes[identity.pubkey].length; i++) {
      arr.push(this.missingAttributes[identity.pubkey][i].name);
    }
    console.log("MissingRefs:" + this.missingReferences[identity.pubkey]);
    for (i = 0; i < this.missingReferences[identity.pubkey].length; i++) {
      arr.push(this.missingReferences[identity.pubkey][i].name);
    }
    return arr;
  }

  getOptional(identity) {
    const arr = [];
    let i = 0;
    if (this.inOpenIdFlow()) {
    for (i = 0; i < this.optionalReferences[identity.pubkey].length; i++) {
      arr.push(this.optionalReferences[identity.pubkey][i].name);
    }
    }
    return arr;
  }

  getMissingPretty(identity) { return this.getMissing(identity).join(', '); }

  canAuthorize(identity) {
    return this.inOpenIdFlow() && !this.isInEdit(identity);
  }

  isRequested(identity, attribute) {
    if (undefined === this.requestedAttributes[identity.pubkey]) {
      return false;
    } else {
      return -1 !==
        this.requestedAttributes[identity.pubkey].indexOf(attribute);
    }
  }

  isRefRequested(identity, reference) {
    if (undefined === this.requestedReferences[identity.pubkey]) {
      return false;
    } else {
      return -1 !==
        this.requestedReferences[identity.pubkey].indexOf(reference);
    }
  }

  isAttrRefRequested(identity, attribute) {
    if (undefined === this.requestedReferences[identity.pubkey]) {
      return false;
    } else {
      for (var j = 0; j < this.requestedReferences[identity.pubkey].length; j++) {
        if (attribute.name === this.requestedReferences[identity.pubkey][j].name) {
          return true;
        }
      }
      return false;
    }
  }

  isoptRefRequested(identity, reference) {
    if (undefined === this.optionalReferences[identity.pubkey]) {
      return false;
    } else {
      return -1 !==
        this.optionalReferences[identity.pubkey].indexOf(reference);
    }
  }

  isAttributeMissing(identity) {
    if (!this.inOpenIdFlow()) {
      return false;
    }
    if (undefined === this.requestedAttributes[identity.pubkey]) {
      return false;
    }
    return this.getScopes().length !==
      this.requestedAttributes[identity.pubkey].length;
  }

  isReferenceMissing(identity) {
    if (!this.inOpenIdFlow()) {
      return false;
    }
    if (undefined === this.requestedReferences[identity.pubkey]) {
      return false;
    }
    for (var i =0; i<this.getRefScope().length; i++) {
      if (this.getRefScope()[i][1] === true) {
        var j;
        for (j =0; j< this.requestedReferences[identity.pubkey].length; j++) {
          if (this.getRefScope()[i][0] === this.requestedReferences[identity.pubkey][j].name){
            break;
          }
        }
        if (j === this.requestedReferences[identity.pubkey].length){
          return true;
        }
      }
    }
    return false;
  }

  isReqReferenceInvalid(identity) {
    if (!this.inOpenIdFlow()) {
      return false;
    }
    if (undefined === this.requestedReferences[identity.pubkey]) {
      return false;
    }
    for (var j =0; j< this.requestedReferences[identity.pubkey].length; j++) {
      if (!this.isAttestationValid(this.requestedReferences[identity.pubkey][j].ref_id, identity)){
        return true;
      }
    }
    return false;
  }

  hasAttributes(identity) {
    if (undefined === this.attributes[identity.pubkey]) {
      return false;
    }
    return 0 !== this.attributes[identity.pubkey].length;
  }

  hasReferences(identity) {
    if (undefined === this.references[identity.pubkey]) {
      return false;
    }
    return 0 !== this.references[identity.pubkey].length;
  }

  hasAttestation(identity) {
    if (undefined === this.attestation[identity.pubkey]) {
      return false;
    }
    return 0 !== this.attestation[identity.pubkey].length;
  }

  private updateIdentities() {
    this.identityService.getIdentities().subscribe(identities => {
      this.identities = [];
      let i;
      this.identityNameMapper = {};
      for (i = 0; i < identities.length; i++) {
        this.identityNameMapper[identities[i].pubkey] = identities[i].name;
        this.identities.push(identities[i]);
        if (this.identityInEditName === identities[i].name) {
          this.editIdentity(this.identities[this.identities.length - 1]);
          this.identityInEditName = '';
        }
      }

      identities.forEach(identity => {
        this.updateAttributes(identity);
        this.updateReferences(identity);
        this.updateAttestation(identity);
      });
      this.closeModal('GnunetInfo');
      this.connected = true;
    },
      error => {
        console.log(error);
        this.openModal('GnunetInfo');
        this.connected = false;
      });
  }

  getAudienceName(ticket) {
    if (undefined === this.identityNameMapper[ticket.audience]) {
      return 'Unknown';
    }
    return this.identityNameMapper[ticket.audience];
  }

  isConnected() {
    return this.connected;
  }

  canSearch() {
    return this.isConnected() && 0 != this.identities.length && !this.isAddIdentity() && (null == this.identityInEdit);
  }
}
